public class Aluno{
	
	private String nome;
	private String matricula;
 
	public Aluno(String umNome, String umaMatricula){
		nome = umNome;
		matricula = umaMatricula;
	}
	
	public void setNome (String umNome){
		nome = umNome;
	}
	public void setMatricula (String umaMatricula){
		matricula = umaMatricula;
	}
	public String getNome(){
		return nome;
	}
	public String getMatricula(){
		return matricula;
	}

}
