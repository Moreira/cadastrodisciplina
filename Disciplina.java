import java.util.ArrayList;

public class Disciplina{
	
	private String disciplina;
	private String codigo;
	private ArrayList<Aluno> listaAlunosDisciplina;
	
	public Disciplina (String umNome, String umCodigo){
		disciplina = umNome;
		codigo = umCodigo;
		listaAlunosDisciplina = new ArrayList<Aluno>();
	}

	public void setDisciplina (String umaDisciplina){
                disciplina = umaDisciplina;
        }

	public void setCodigo (String umCodigo){
		codigo = umCodigo;
	}

	public String getDisciplina(){
                return disciplina;
        }

	public String getCodigo(){
		return codigo;
	}
	
	public void adicionar (Aluno umAluno){
		listaAlunosDisciplina.add(umAluno);
	}

	public void remover (Aluno umAluno){
		listaAlunosDisciplina.remove(umAluno);
	}

}
