import java.util.ArrayList;

public class ControleAluno{

	private ArrayList<Aluno> listaAlunos;
	
		public ControleAluno(){
		listaAlunos = new ArrayList<Aluno>();
	}

	public void adicionarAluno (Aluno umAluno){
		listaAlunos.add(umAluno);
	}

	public void removerAluno (String umNome){
		for (Aluno umAluno: listaAlunos){
			if (umAluno.getNome().equalsIgnoreCase(umNome)){
				listaAlunos.remove(umAluno);
			}
		}		
	}
	
	public Aluno pesquisarNomeAluno (String umNome){
	
		for(Aluno umAluno : listaAlunos){
			if(umAluno.getNome().equalsIgnoreCase(umNome)){
				return umAluno;
			}
		}
		return null;
	}


}
